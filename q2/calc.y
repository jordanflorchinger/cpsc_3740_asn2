%{
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#define YYSTYPE double
%}

%token NUMBER
%token PLUS MINUS TIMES DIVIDE POWER
%token FLR CEIL ABS SQRT SIN COS TAN LOG LOG10
%token LEFT RIGHT
%token END

%left PLUS MINUS
%left TIMES DIVIDE
%left NEG
%left FLR CEIL ABS SQRT SIN COS TAN LOG LOG10
%right POWER

%start Input
%%

Input:

     | Input Line
;

Line:
     END
     | Expression END { printf("Result: %f\n", $1); }
;

Expression:
     NUMBER { $$=$1; }
| Expression PLUS Expression { $$=$1+$3; }
| Expression MINUS Expression { $$=$1-$3; }
| Expression TIMES Expression { $$=$1*$3; }
| Expression DIVIDE Expression { $$=$1/$3; }
| MINUS Expression %prec NEG { $$=-$2; }
| Expression POWER Expression { $$=pow($1,$3); }
| FLR LEFT Expression RIGHT { $$=floor($3); }
| CEIL LEFT Expression RIGHT { $$=ceil($3); }
| ABS LEFT Expression RIGHT { $$=abs($3); }
| SQRT LEFT Expression RIGHT { $$=sqrt($3); }
| SIN LEFT Expression RIGHT { $$=sin($3); }
| COS LEFT Expression RIGHT { $$=cos($3); }
| TAN LEFT Expression RIGHT { $$=tan($3); }
| LOG LEFT Expression RIGHT { $$=log($3); }
| LOG10 LEFT Expression RIGHT { $$=log10($3); }
| LEFT Expression RIGHT { $$=$2; }
;

%%

int yyerror(char *s) {
  printf("%s\n", s);
}

int main() {
  if (yyparse())
     fprintf(stderr, "Successful parsing.\n");
  else
     fprintf(stderr, "error found.\n");
}
