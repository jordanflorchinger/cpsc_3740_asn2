%{
#define YYSTYPE double
#include "calc.tab.h"
#include <stdlib.h>
%}

white [ \t]+
digit [0-9]
integer {digit}+
exponent [eE][+-]?{integer}
real {integer}("."{integer})?{exponent}?

%%

{white} { }
{real} { yylval=atoi(yytext);
 return NUMBER;
}

"log10" return LOG10;
"sqrt" return SQRT;
"log" return LOG;
"sin" return SIN;
"cos" return COS;
"tan" return TAN;
"floor" return FLR;
"ceil" return CEIL;
"abs" return ABS;
"+" return PLUS;
"-" return MINUS;
"*" return TIMES;
"/" return DIVIDE;
"^" return POWER;
"(" return LEFT;
")" return RIGHT;
"\n" return END;
