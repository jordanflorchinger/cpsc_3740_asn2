%{
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define YYSTYPE double
%}

%token NUMBER
%token PLUS MINUS TIMES DIVIDE POWER
%token FLR CEIL ABS SQRT SIN COS TAN LOG LOG10
%token LEFT RIGHT
%token END

%left PLUS MINUS
%left TIMES DIVIDE
%left NEG
%left FLR CEIL ABS SQRT SIN COS TAN LOG LOG10
%right POWER

%start Input
%%

Input:

     | Input Line
;

Line:
     END
     | Expression END { printf("\n"); }
;

Expression:
     NUMBER { printf("%d ", yylval); }
| Expression PLUS Expression { printf("+ "); }
| Expression MINUS Expression { printf("- "); }
| Expression TIMES Expression { printf("* "); }
| Expression DIVIDE Expression { printf("/ "); }
| MINUS Expression %prec NEG { $$=-$2; }
| Expression POWER Expression { printf("^ "); }
| FLR LEFT Expression RIGHT { printf("flr "); }
| CEIL LEFT Expression RIGHT { printf("ceil "); }
| ABS LEFT Expression RIGHT { printf("abs "); }
| SQRT LEFT Expression RIGHT { printf("sqrt "); }
| SIN LEFT Expression RIGHT { printf("sin "); }
| COS LEFT Expression RIGHT { printf("cos "); }
| TAN LEFT Expression RIGHT { printf("tan "); }
| LOG LEFT Expression RIGHT { printf("log "); }
| LOG10 LEFT Expression RIGHT { printf("log10 "); }
| LEFT Expression RIGHT { $$=$2; }
;

%%

int yyerror(char *s) {
  printf("%s\n", s);
}

int main() {
  if (yyparse())
     fprintf(stderr, "Successful parsing.\n");
  else
     fprintf(stderr, "error found.\n");
}
